import "dart:collection";

main() {
  final SplayTreeMap<String, Map<String,String>> app = 
      SplayTreeMap<String, Map<String,String>>();

  app["FNB"] = {"winner" : "2012"};
  app["Nedbank"] = {"winner" : "2013"};
  app["SuperSport"] = {"winner" : "2014"};
  app["Dstv Now"] = {"winner" : "2015"};
  app["Tuta-me"] = {"winner" : "2016"};
  app["OrderIn"] = {"winner" : "2017"};
  app["BesMarta.com"] = {"winner" : "2018"};
  app["Vula"] = {"winner" : "2019"};
  app["Chechers 60"] = {"winner" : "2020"};
  app["Takealot"] = {"winner" : "2021"};
  
 

  for (final String key in app.keys) {
    print("$key : ${app[key]}");
    //print(app);
    
  }
  var reversed = Map.fromEntries(app.entries.map((e) => MapEntry(e.value, e.key)));
  for (var kv in reversed.entries) {
    print(kv);
  }
   var len = app.length;
  print('Total number of apps : $len');
}
 
      

